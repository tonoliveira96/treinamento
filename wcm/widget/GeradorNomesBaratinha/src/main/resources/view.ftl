<div id="GeradorNomesBaratinha_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide" data-params="GeradorNomesBaratinha.instance()">
	<section>
        <h1>Esse é o incrível gerador de nomes legais</h1>
        <p>Clique no botão para descobrir um nome legal:</p>
         <button class="btn btn-default btn-lg" data-gerarNome>Gerar nome!</button>
        <div id="nomeContainer">
            <h3>O nome da sua baratinha é <span id="nomeGerado"></span></h3>
        </div>
    </section>
</div>

