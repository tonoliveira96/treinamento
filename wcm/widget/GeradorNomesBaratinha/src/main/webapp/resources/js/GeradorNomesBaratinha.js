var GeradorNomesBaratinha = SuperWidget.extend({
  //método iniciado quando a widget é carregada

  nomes: [
    "Gertrudes",
    "Maria Josefina",
    "Joana",
    "Ana",
    "Leonarda",
    "Jurubeba",
    "Maria Bonita",
    "Carolina",
    "Lilica",
    "Casquinha",
    "Xantófila",
    "Cuca",
    "Biritinha",
    "Ana Bonitinha",
    "Duqueza",
    "Zizi",
    "Gigi",
    "Pirata",
    "Judite",
    "Jessie",
    "Lulu",
    "Lili",
    "Zuzu",
    "Amelia",
    "Bililica",
    "Biricutica",
  ],
  init: function () {},

  //BIND de eventos
  bindings: {
    local: {
      gerarNome: ["click_funcGerarNome"],
    },
    global: {},
  },
  funcGerarNome: function (el, ev) {
    let nomeAleatorio =
      this.nomes[Math.floor(Math.random() * this.nomes.length)];
    $("#nomeGerado").html(nomeAleatorio);
    $("#nomeContainer").show();
  },
});
